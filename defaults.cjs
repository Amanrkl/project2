// Fill in undefined properties that match properties on the `defaultProps` parameter object.
// Return `obj`.

let keys = require('./keys.cjs');

function defaults(obj,defaultProps) {
  let _keys = keys(obj);
  let length = arguments.length;
  obj = Object(obj);
  if (length < 2 || obj == null) 
    {return obj;}

  for (let index = 1; index < length; index++) {
    let source = arguments[index],
    keys_source = keys(source),
    l = keys_source.length;
    for (var i = 0; i < l; i++) {
      var key = keys_source[i];
      if (obj[key] === void 0) 
        {obj[key] = source[key];}
      }
    }
    return obj;
  }
module.exports = defaults