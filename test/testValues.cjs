const values = require('../values.cjs')

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham'}; 
const testObject2 = { name: 'Bruce Wayne', age: [2,3], location: function(){ console.log('Gotham')}}; 


const result = values(testObject1)
console.log(result);
console.log(values(testObject2));
console.log(values([1,2]));
console.log(values());
console.log(values(34));