// Convert an object into a list of [key, value] pairs.

let keys = require('./keys.cjs');

function pairs(obj) {
  let _keys = keys(obj);
  let length = _keys.length;
  let pairs = Array(length);
  for (let i = 0; i < length; i++) {
    pairs[i] = [_keys[i], obj[_keys[i]]];
  }
  return pairs;
}

module.exports = pairs