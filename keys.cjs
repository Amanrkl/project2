// Retrieve all the names of the object's properties.

function keys(obj) {
    const type = typeof obj;
    if (!(type === 'object' && !!obj)) {
        return [];
    }
    if (Array.isArray(obj)){
        return []
    }
    let keys = [];
    for (let key in obj) {
        if (obj.hasOwnProperty(key)){
            keys.push(key);
        }
    }
    return keys;
  }
  
module.exports = keys