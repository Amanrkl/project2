// Transform the value of each property in turn by passing it to the callback function.

let keys = require('./keys.cjs');

function mapObject(obj, cb) {
  let _keys = keys(obj),
      length = _keys.length,
      results = {};
  if (!( typeof cb === 'function')){
    return obj
  }
  for (let index = 0; index < length; index++) {
    let currentKey = _keys[index];
    results[currentKey] = cb(obj[currentKey], currentKey);
  }
  return results;
}

module.exports = mapObject