// Return all of the values of the object's own properties.
// Ignore functions

let keys = require('./keys.cjs');

function values(obj) {
  let _keys = keys(obj);
  let length = _keys.length;
  let values = [];
  for (let i = 0; i < length; i++) {
    if (!(typeof(obj[_keys[i]]) === 'function')){
        values.push(obj[_keys[i]]);
    }
  }
  return values;
}

module.exports = values