// Returns a copy of the object where the keys have become the values and the values the keys.

let keys = require('./keys.cjs');

function invert(obj) {
  let result = {};
  let _keys = keys(obj);
  for (let i = 0, length = _keys.length; i < length; i++) {
    result[obj[_keys[i]]] = _keys[i];
  }
  return result;
}

module.exports = invert